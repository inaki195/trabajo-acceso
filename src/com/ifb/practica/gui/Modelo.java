package com.ifb.practica.gui;

import com.ifb.practica.base.Club;
import com.ifb.practica.base.Cuerpotecnico;
import com.ifb.practica.base.Jugador;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;

public class Modelo {

    private HashSet<Club> club;
    private HashSet<Cuerpotecnico>cuerpotecnico;
    private HashSet<Jugador>jugadores;

    /**
     *
     */
    public Modelo() {
        club=new HashSet<>();
        cuerpotecnico=new HashSet<>();
        jugadores=new HashSet<>();
    }



    public HashSet <Club> getClub() {
        return club;
    }

    /**
     *
     *
     * @return cuerpotecnico
     */
    public HashSet <Cuerpotecnico> getCuerpotecnico() {
        return cuerpotecnico;
    }

    /**
      *obtiene el hashet  de jugadores
     @return Jugador {@link HashSet}
     */
    public HashSet <Jugador> getJugadores() {
        return jugadores;
    }
        //recibe atributos que se añaden a un objeto y ese objeto se añade hashet <>de club

    /**
     * te genera un nuevo club
      @param nombre String
      @param fundacion localdate
      @param titulos int
     */
    public void nuevoClub(String nombre,LocalDate fundacion, int titulos){
        club.add(new Club(nombre,fundacion,titulos));
    }
    //elimina un club de del hashet<>del club

    /**
     * elimina un club del hasshet
      @param clubes {@link Object}
     */
    public void  eliminarClub(Club clubes){
        club.remove(clubes);
    }

    /**
     *  add un objeto de cuerpotecnico al Hashet
      @param nombre     {@link String}
      @param apellido {@link String}
    @param ocupacion {@link String}
      @param salario {@link Float}
      @param edad {@link Integer}
     */
    public void nuevoCuerpotecnico(String nombre,String apellido,String ocupacion,Float salario,int edad){
        cuerpotecnico.add(new Cuerpotecnico(nombre,apellido,ocupacion,salario,edad));
    }


    /**
     * eliminar cuerpotecnico del hashet
     @param cuerpotecnicos {@link Object}
     */
    public void  eliminarCuerpotecnico(Cuerpotecnico cuerpotecnicos){

        cuerpotecnico.remove(cuerpotecnicos);
    }
    //

    /**
      *crea jugador en la lista de disponibles
     @param nombre {@link String}
      @param salario {@link Float}
      @param dorsal {@link Integer}
     @param nacionalidad {@link String}
     @param edad {@link Integer}
     */
    public void nuevoJugador(String nombre,float salario,int dorsal,String nacionalidad,int edad){
        jugadores.add(new Jugador(nombre,salario,dorsal,nacionalidad,edad));
    }
    //elimina un jugador del hashet<jugador>

    /**
       elimina un jugador de la lista
      @param jugador {@link Object}
     */
    public void eliminarJugador(Jugador jugador){
        jugadores.remove(jugador);
    }


    /**
     * guardar toda la informacion de los hasshet en un fichero
     *
     */
    public void guardarDatosEnFichero(){

        try {
            FileOutputStream flujofichero=new FileOutputStream("datos.bin");
            ObjectOutputStream serializador=new ObjectOutputStream(flujofichero);
            serializador.writeObject(club);
            serializador.writeObject(cuerpotecnico);
            serializador.writeObject(jugadores);
            flujofichero.close();
        } catch (IOException e) {
            e.printStackTrace ();
        }

    }

    /**
      * obtiene los datos de un fichero y rellena los datos en los correspondientes hashet
     */
    public void cargarDatosFichero() {

        FileInputStream flujofichero= null;
        try {
            flujofichero = new FileInputStream ("datos.bin");
            ObjectInputStream deserializador=new ObjectInputStream(flujofichero);
            club= (HashSet<Club>) deserializador.readObject();
            cuerpotecnico= (HashSet<Cuerpotecnico>) deserializador.readObject();
            jugadores= (HashSet<Jugador>) deserializador.readObject();
            flujofichero.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace ();
        } catch (IOException e) {
            e.printStackTrace ();
        } catch (ClassNotFoundException e) {
            e.printStackTrace ();
        }

    }

}
