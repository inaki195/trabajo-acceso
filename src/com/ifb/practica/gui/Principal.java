package com.ifb.practica.gui;

public class Principal {
    /**
     * metodo que ejecuta el programa
     * @param args {@link String[]}
     */
    public static void main(String[] args) {
        Vista vista =new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(vista,modelo);
    }
}
