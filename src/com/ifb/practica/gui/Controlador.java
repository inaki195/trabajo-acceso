package com.ifb.practica.gui;

import com.ifb.practica.base.Club;
import com.ifb.practica.base.Cuerpotecnico;
import com.ifb.practica.base.Jugador;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.IOException;
import java.time.LocalDate;

import static java.lang.Integer.parseInt;

/**
 *
 */
public class Controlador implements ActionListener,KeyListener ,ListSelectionListener, WindowListener,MouseListener {

private Vista vista;
private Modelo modelo;
        //constructor que comunica la vista con el modelo para qu puedo interactuar

    /**
     comunicar la clase vista con la clase modelo  y add listener al teclado a la ventana  y  a seleccion
     de los Jlist
     @param vista   {@link Object}
     @param modelo {@link Object}
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        addActionListener(this);
        addkeyListener( this );
        addSelectionListener ( this );
        addWindowsListeners(this);
    }

    /**
     * add un windowsListener al frame(ventana)para escuchar cuandp
     * se abre o cirra la ventana
     * @param listener {@link WindowListener}
     */
    private void addWindowsListeners(WindowListener listener) {
        vista.frame.addWindowListener(listener);

    }
        //añado botones de escucha a todas la listas

    /**
     * add a los JList  para que puedan escuchar de teclado
     * @param controlador {@link KeyListener}
     */
    private void addkeyListener(KeyListener controlador) {
        vista.listajugadores.addKeyListener(controlador);
        vista.listacuerpotecnico.addKeyListener(controlador);
        vista.listaequipos.addKeyListener(controlador);
    }

    private void addSelectionListener(Controlador controlador) {
    }
        /**
        *   listener add listerner a todos los bototones
        * @param listener {@link ActionListener}
         */
    private void addActionListener(ActionListener listener){
        //add listener a los todos los botones de añadir
    vista.BTNAddCuerpotecnico.addActionListener(listener);
    vista.BTNAddJugador.addActionListener (listener);
    vista.BTNAnyadirClub.addActionListener (listener);
    //añadir listeneer A los botones de aplicar
    vista.BTNAplicarCueerpoTecnico.addActionListener(listener);
    vista.BTNAplicarClub.addActionListener(listener);
    vista.BTNaplicarJugador.addActionListener(listener);
    //add listener a todos los botones eliminar
    vista.BTNeliminarcuerpoTecnico.addActionListener(listener);
    vista.BTNEliminarClub.addActionListener (listener);
    vista.BTNeliminarJugador.addActionListener (listener);
    //add los listener a obtener informacion
        vista.BTNobtenerINFOCuerpoTecnico.addActionListener(listener);
        vista.BTNObtenerInfoClub.addActionListener (listener);
        vista.BTNInfoJugador.addActionListener (listener);
        vista.BTNrellenarClub.addActionListener(listener);

        //mostrar informacio del club
        vista.BTNInfoClub.addActionListener ( listener );


    }
    @Override
    /**
     * cada boton tiene un actioncommand el cual contiene un string
     *     dicho string se obtiene y depende cual sea ejecuta unos metodos  u otros
     */
    public void actionPerformed(ActionEvent e) {
        String comando=e.getActionCommand ();
        switch (comando){
            case "motrarinformacion":{
                listarJugadoresClub ();
                listarCuerpoTecnicoClub ();

            }
            break;
            //coge todos los campos y los convierte en atributos del club para generar un objeto
            // dicho objeto sera añadido a lista y despues se actualizara una vez mostrado los campos se borran
            case "anyadirclub": {
                String nombre = vista.txtnombreclub.getText();
                int titulo = parseInt(vista.txttitulos.getText());
                LocalDate fecha = vista.fechainaguracion.getDate();

                modelo.nuevoClub(nombre, fecha, titulo);
                listarClubes();
                limpiarCampos();
            }
            break;
            /**
            * coge todos los campos y los convierte en atributos del cuerpoTecncio para generar un objeto
          * dicho objeto sera añadido a lista y despues se actualizara una vez mostrado los campos se borran
             */
            case "addCuerpoTecnicos":{
                String nombre=vista.txtnombreCuerpoTecnico.getText();
                String apellido=vista.txtapellidoCuerpoTecnico.getText();
                String ocupacion=vista.txtocupacionCuerpoTecnico.getText();
                float salario=(Float.parseFloat(vista.txtsalarioCuerpoTecnico.getText()));
                int edad= (int) Float.parseFloat(vista.txtedadCuerpoTecnico.getText());
                modelo.nuevoCuerpotecnico(nombre,apellido,ocupacion,salario,edad);
                listarCuerpoTecnico();
                limpiarCuerpoTecnico();
            }
            break;
            //coge todos los campos y los convierte en atributos del jugador para generar un objeto
            // dicho objeto sera añadido a lista y despues se actualizara una vez mostrado los campos se borran
            case "anyadirjugador":{
                System.out.println("add player");
                //String nombre, float equipo, int dorsal, String nacionalidad, float altura
                String nombre=(vista.txtnombreJugador.getText());
                int dorsal=(Integer.parseInt(vista.txtdorsalJugador.getText()));
                float salario=(Float.parseFloat(vista.txtSalarioJugador.getText()));
                String nacionalidad=(vista.txtnacionalidad.getText());
                int edad=(Integer.parseInt(vista.txtedadJugador.getText()));
                //String nombre,float salario,int dorsal,String nacionalidad,int edad
                modelo.nuevoJugador(nombre,salario,dorsal,nacionalidad,edad);
                listarJugadoes();
                limpiarJugadores();
            }
            //
            /**
             * se selecciona a cierto jugador de cual se elimina el objeto (Jugador)
             *  para posteriormente se eliminado del hashet del modelo .Al el eliminarlo actualizamos la lista
             */
            case "eliminarjugador":{
                Jugador jugador= (Jugador) vista.listajugadores.getSelectedValue();
                modelo.eliminarJugador(jugador);
                listarJugadoes();

            }
            break;

            /**
             *   se selecciona un objeto de jugador y te cambia todos los campos con su informacion correspondiente
             */
            case "obtenerinfojugador":{
                Jugador jugador= (Jugador) vista.listajugadores.getSelectedValue();
                vista.txtnombreJugador.setText(jugador.getNombre());
                vista.txtdorsalJugador.setText(String.valueOf(jugador.getDorsal()));
                vista.txtSalarioJugador.setText(String.valueOf(jugador.getSalario()));
                vista.txtedadJugador.setText(String.valueOf(jugador.getAltura()));
                vista.txtnacionalidad.setText(String.valueOf(jugador.getNacionalidad()));
            }
            break;

            /**
             *  se selecciona a un jugador y despues de haber obtenido su informacion podremos
             *             cambiar el campo seleccionado que quieras al aplicar(boton) se cambiaran los cambios
             */
            case "aplicarjugador":{

               Jugador jugador= (Jugador) vista.listajugadores.getSelectedValue();
               String nombre=vista.txtnombreJugador.getText();
               int dorsal= parseInt(vista.txtdorsalJugador.getText());
                String nacionalidad=vista.txtnacionalidad.getText();
                float salario=Float.parseFloat(vista.txtSalarioJugador.getText());
                float altura=Float.parseFloat(vista.txtedadJugador.getText());jugador.setNombre(nombre);
               jugador.setDorsal(dorsal);
               jugador.setSalario(salario);
               jugador.setAltura(altura);
               jugador.setNacionalidad(nacionalidad);
               jugador.setNombre(nombre);
               limpiarJugadores();
               listarJugadoes();
            }
            break;

            /**
             * coge de la lista un objeto que se envia a un metodo que eliminar un objeto Club  del hasshet
             * posteriormente se actualiza la lista
             */
            case "delete":
                Club club= (Club) vista.listaequipos.getSelectedValue();
                modelo.eliminarClub(club);
                listarClubes();
                break;

            /**
             *  se selecciona a un club y despues de haber obtenido su informacion podremos
             *  cambiar el campo seleccionado que quieras al aplicar(boton) se cambiaran los cambios
             */
            case"aplicarcambiosclub": {

                Club club1= (Club) vista.listaequipos.getSelectedValue();
                LocalDate fecha=vista.fechainaguracion.getDate();
                int titulos= parseInt(vista.txttitulos.getText());
                String nombrecf=vista.txtnombreclub.getText();
                    club1.setAnioInaguracion(fecha);
                    club1.setTitulos(titulos);
                    club1.setNombreCF(nombrecf);
                limpiarCampos();
                listarClubes();
            }
              break;
            //
            /**
             * se obtiene en los campos correspondientes los respectivos atributos del
             *             club seleccionado en la lista
             */
            case "obtener informacion": {
                Club club1 = (Club) vista.listaequipos.getSelectedValue();
                LocalDate date = club1.getAnioInaguracion();
                vista.fechainaguracion.setDate(club1.getAnioInaguracion());
                vista.txtnombreclub.setText(club1.getNombreCF());
                vista.txttitulos.setText(String.valueOf(club1.getTitulos()));
            }
                break;


            /**
             * borra el objeto (CuerpoTecnico) del hasshet correspondiente
             *             y despues se actualiza la lista para comprobar que esta borrado
             */
            case "eliminarcuerpoTecnico":{
                Cuerpotecnico cuerpotecnico= (Cuerpotecnico) vista.listacuerpotecnico.getSelectedValue();
                modelo.eliminarCuerpotecnico(cuerpotecnico);
                listarCuerpoTecnico();
            }
            break;

            /**
             * se obtiene  todas la propiedades del cuerpoTencio los correspondientes campos
             */
            case "obtenerinfoTecnico":{
                Cuerpotecnico cuerpotecnico= (Cuerpotecnico) vista.listacuerpotecnico.getSelectedValue();
                vista.txtnombreCuerpoTecnico.setText(cuerpotecnico.getNombre());
                vista.txtapellidoCuerpoTecnico.setText(cuerpotecnico.getApellido());
                vista.txtocupacionCuerpoTecnico.setText(cuerpotecnico.getOcupacion());
                vista.txtsalarioCuerpoTecnico.setText(String.valueOf(cuerpotecnico.getSalario()));
                vista.txtedadCuerpoTecnico.setText(String.valueOf(cuerpotecnico.getEdad()));
            }
            break;
            //
            /**
             * se cambia los atributos del objet(cuerpoTecncio seleccionado en la lista)
             *             y despues dandale a boto aplicar se guardar los cambios
             *            y se limpian los campos
             */
            case "modificarcuerpotecnico":{
                Cuerpotecnico cuerpotecnico= (Cuerpotecnico) vista.listacuerpotecnico.getSelectedValue();
                String nombre=vista.txtnombreCuerpoTecnico.getText();
                String apellido=vista.txtapellidoCuerpoTecnico.getText();
                String ocupacion=vista.txtocupacionCuerpoTecnico.getText();
                float salario=Float.parseFloat(vista.txtsalarioCuerpoTecnico.getText());
                int edad=Integer.parseInt(vista.txtedadCuerpoTecnico.getText());
                cuerpotecnico.setApellido(apellido);
                cuerpotecnico.setNombre(nombre);
                cuerpotecnico.setOcupacion(ocupacion);
                cuerpotecnico.setSalario(salario);
                cuerpotecnico.setEdad(edad);
                limpiarCuerpoTecnico();
                listarCuerpoTecnico();

            }
            break;

            /**
             *   te abrira  un jdialog para la administracion de dicho club y poder add tanto cuerpo tecnico
             *            como jugadores
             */
            case "seleccionar personal":{
                Club club1= (Club) vista.listaequipos.getSelectedValue();
                Administrarclub dialog=new Administrarclub(club1,modelo.getJugadores(),modelo.getCuerpotecnico());
                dialog.mostrarDiaologo();
                listarPersonal(club1);


            }
            break;
        }

    }
    /**
     *  te muestra  tanto los jugadores como el cuerpo tecnico que pertenece
     *     ha dicho club seleccionado
     * @param club1 {@link Object}
     */
    private void listarPersonal(Club club1) {
        vista.dlmcuerpotecnico.clear();
        for(Jugador jugador:club1.getListaJugador()){
            vista.dlmjugadores.addElement(jugador);
        }
        vista.dlmcuerpotecnico.clear();
        for(Cuerpotecnico cuerpotecnico:club1.getListaCuerpoTecnico()){
            vista.dlmcuerpotecnico.addElement(cuerpotecnico);
        }

    }
/*
 */

    /** limpia el dlm para posteriormente recorrer hasshet donde anteriormente se han add a los clubes
     .Posteriormente se recorre dicho hashet y add a la lista
     *
     */
    private void listarClubes() {
        vista.dlmclub.clear();
        for(Club club:modelo.getClub ()){
        vista.dlmclub.addElement(club);
        }
    }
    /*
     */

    /**
     * te muestra todos los jugadoes estan en lista general .
     *         se vacia el dlm para posteriomerte recorrer el hasshet (donde se encuntra los jugadores)
     *         y añadirlo al dlm
     */
    private void listarJugadoes(){
        vista.dlmjugadores.clear();
        for(Jugador jugador:modelo.getJugadores()){
            vista.dlmjugadores.addElement(jugador);
        }
    }

    /**
     * te muestra todos la gente del cuerpo tecnico en general
     *         limpiar el defaultModel correspondiente  y recorrre un hasshet
     *         que uno a uno los va add al defautlmodel correspondiente
     */
    private void listarCuerpoTecnico(){
        vista.dlmcuerpotecnico.clear();
        for (Cuerpotecnico cuerpotecnico:modelo.getCuerpotecnico()){
            vista.dlmcuerpotecnico.addElement(cuerpotecnico);
        }
    }


    /**  te muestra en un jlit todos los object jugadores que se almacenan en un array que el parametro
     * del club
     *
     */
    private void listarJugadoresClub(){

            vista.dlmjugadoresClub.clear ();
        Club seleccionado= (Club) vista.listaequipos.getSelectedValue ();

        for(Jugador jugador:seleccionado.getListaJugador ()){
            vista.dlmjugadoresClub.addElement ( jugador );
        }
    }

    /**
     *  te muestra en un jlit todos los object de cuerpotecnico que se almacenan en un array que el parametro
     * del club
     * q
     */
    private void listarCuerpoTecnicoClub() {
        vista.dlmcuerpotecnicoClub.clear ();
        Club seleccionado= (Club) vista.listaequipos.getSelectedValue ();
        for(Cuerpotecnico cuerpotecnico:seleccionado.getListaCuerpoTecnico ()){
            vista.dlmcuerpotecnicoClub.addElement ( cuerpotecnico );
        }
    }

    /**
     *  limpia todo los textfield que pertenecen al cuerpo tecncio
     */
    private void limpiarCuerpoTecnico(){
        vista.txtnombreCuerpoTecnico.setText("");
        vista.txtapellidoCuerpoTecnico.setText("");
        vista.txtocupacionCuerpoTecnico.setText("");
        vista.txtsalarioCuerpoTecnico.setText("");
        vista.txtedadCuerpoTecnico.setText("");

    }


    /**
     *  limpia todos los textfield de jugadores
     */
    private void limpiarJugadores(){
        vista.txtnombreJugador.setText("");
        vista.txtSalarioJugador.setText("");
        vista.txtedadJugador.setText("");
        vista.txtnacionalidad.setText("");
        vista.txtdorsalJugador.setText("");
    }

    /**
     *  limpia todos  los campos que pertenece al area
     */
    private void limpiarCampos() {
        vista.fechainaguracion.clear();
        vista.txtnombreclub.setText ( "" );
        vista.txttitulos.setText ( "" );

    }



    @Override
    /** @use cuando se abro la ventana este metodo se acciona
        y me llama al metodo  de cargar los datos
        para posteriomente mostrarme las listas del club,cuerpo tecnico y los jugadores
     */
    public void windowOpened(WindowEvent e) {
        modelo.cargarDatosFichero();
        listarCuerpoTecnico();
        listarJugadoes();
        listarClubes();

    }

    @Override
    /**
      * cuando se cierra la ventana este metodo se acciona y me llama al metodo  de guardar los datos
        *en el fichero binario
     */
    public void windowClosing(WindowEvent e) {
               modelo.guardarDatosEnFichero();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
