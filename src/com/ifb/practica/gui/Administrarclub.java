package com.ifb.practica.gui;

import com.ifb.practica.base.Club;
import com.ifb.practica.base.Cuerpotecnico;
import com.ifb.practica.base.Jugador;

import javax.swing.*;
import java.awt.event.*;
import java.util.HashSet;

public class Administrarclub extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList listajugadoractual;
    private JList listajugadoresdisponibles;
    private JList listacuerpotecnicoactual;
    private JList listacuerpotecnicodisponible;
    private JButton BTNQuitarJugadores;
    private JButton BTNAddJugadoresClub;
    private JButton BTNQuitarCuerpoTecnico;
    private JButton BTNAddCuerpoTecnicoClub;
    private JButton buttonCancel;
    private Club club;
    private DefaultListModel<Jugador>dlmjugadoesDisponibles;
    private DefaultListModel<Jugador>dlmjugadoresCogido;
    private DefaultListModel<Cuerpotecnico>dlmcuerpoTecnicoDisponibles;
    private DefaultListModel<Cuerpotecnico>dlmcuerpoTecnicocogido;
    private  HashSet<Jugador>jugadoresDisponibles;
    private HashSet<Jugador>jugadorCogido;
    private HashSet<Cuerpotecnico>CuerpoTecnicoCogido;
    private HashSet<Cuerpotecnico>cuerpotecnicosDisponible;
            //jdialog que administra un club
    //

    /**
      creacion del constructor
      @param club1 {@link Object}
      @param jugadores {@link HashSet}
      @param cuerpotecnico {@link HashSet}
     */
    public Administrarclub(Club club1, HashSet<Jugador> jugadores, HashSet<Cuerpotecnico> cuerpotecnico) {
        this.club=club1;
        this.jugadoresDisponibles=jugadores;
        this.cuerpotecnicosDisponible=cuerpotecnico;

        setContentPane(contentPane);
        setTitle(club1.getNombreCF());
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        //boton de añadir jugadores
        /** @use se escoge un jugador(objeto) en la lista de jugadores disponibles para añadirlo
        * al array que es atributo del elquipo y despues la lista se actualizar
         * @param ActionListener
         */
        BTNAddJugadoresClub.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Jugador jugador= (Jugador) listajugadoresdisponibles.getSelectedValue();
                club.getListaJugador().add(jugador);
                listarJugadoresClub();
            }
        });

        /**
         *seleccionamos jugador de la lista
         *  quitar a los jugadores que estan en el array de lista
         *  de jugadores que esta como atributo del objetc club
         * @param ActionListener
         */
        BTNQuitarJugadores.addActionListener( new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {

                Jugador jugador= (Jugador) listajugadoractual.getSelectedValue();
                club.getListaJugador().remove(jugador);
                listarJugadoresClub();
            }
        });
        //
        /**
         * escogemos en la lista de disponibles que Cuerpo tecnico queremos añadir
         *  añade el cuerpotecnico que estan en el array de lista de CuerpoTecnicos
         *  que esta como atributo del objetc club
         *  y actualiza la lista de los cuerpo tecnico del club
         * @param ActionListener
         */
        BTNAddCuerpoTecnicoClub.addActionListener ( new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cuerpotecnico cuerpotecnico1= (Cuerpotecnico) listacuerpotecnicodisponible.getSelectedValue ();
                club.getListaCuerpoTecnico ().add ( cuerpotecnico1 );
                listarCuerpoTecnicoEquipo ();
            }
        } );
        //
        /**
         * quita el cuerpotecnico que estan en el array de lista de CuerpoTecnicos
         * que esta como atributo del objetc club
         *y actualiza la lista de los cuerpo tecnico del club
         * @param ActionListener
         */
        BTNQuitarCuerpoTecnico.addActionListener ( new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cuerpotecnico cuerpotecnico1= (Cuerpotecnico) listacuerpotecnicodisponible.getSelectedValue ();
                club.getListaCuerpoTecnico ().remove ( cuerpotecnico1 );
                listarCuerpoTecnicoEquipo ();

            }
        } );


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        //asigna cada jlist a su correspondiente defaultListModel
        iniciarLista();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {

                onCancel();
            }
        });

        // call onCancel() on ESCAPE

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * @deprecated listaba los jugadores dentro del club
     */
    private void listarJugadoresDeEquipo() {
        dlmjugadoresCogido.clear();
        for(Jugador jugador:club.getListaJugador()){
           dlmjugadoresCogido.addElement(jugador);
        }
    }
    //en este metodo asignamos cada Jlist a un defautlistmodel

    /**
     *  asigna ca Jlist con determinado defaultlistmodel
     */
    private void iniciarLista() {
      //  dlmcuerpoTecnicoActual=new DefaultListModel<>();
      // dlmcuerpoTecnicoDisponibles=new DefaultListModel<>();
        dlmjugadoesDisponibles=new DefaultListModel<>();
        dlmjugadoresCogido=new DefaultListModel<>();

        listajugadoresdisponibles.setModel(dlmjugadoesDisponibles);
        listajugadoractual.setModel(dlmjugadoresCogido);

        dlmcuerpoTecnicoDisponibles=new DefaultListModel<>();
        dlmcuerpoTecnicocogido=new DefaultListModel<>();

        listacuerpotecnicoactual.setModel(dlmcuerpoTecnicocogido);
         listacuerpotecnicodisponible.setModel(dlmcuerpoTecnicoDisponibles);


        listajugadoractual.setModel(dlmjugadoresCogido);
    buttonOK.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            onOK();
        }
    });

    }

    /**
     *  llama al metodo dispose()
     */
    private void onOK() {
        dispose();
    }

    /**
     * add your code here if necessary
     */
    private void onCancel() {
        //
        dispose();
    }
    //

    /**
      lista tanto los jugadores disponibles como el cuerpo tecnico disponible
     */
    public void  mostrarDiaologo(){
        listarJugadores();
        listarCuerpoTecnico();
        setVisible(true);
    }


    /**
     lista el cuerpo tecnico en general tanto los que estan en un equipo como los que no
     */
    private void listarCuerpoTecnico() {
        dlmcuerpoTecnicoDisponibles.clear();
        for(Cuerpotecnico cuerpotecnico:cuerpotecnicosDisponible){
            dlmcuerpoTecnicoDisponibles.addElement(cuerpotecnico);
        }
        listarCuerpoTecnicoEquipo();
    }

    /**
     *  lista a todos los jugadores que estan en lista principal
     */
    private void listarJugadores() {
        //listamos todos los jugadores disponibles
        dlmjugadoesDisponibles.clear();
        for(Jugador jugador:jugadoresDisponibles){
            dlmjugadoesDisponibles.addElement(jugador);
        }
        //listamos los jugadores que pertenece a ese club
        listarJugadoresClub();
    }

    /**
     *  lista todo los jugadores que se encuentre dentro del club
     */
    private void listarJugadoresClub() {
        //
        dlmjugadoresCogido.clear();
        for(Jugador jugador:club.getListaJugador()){
            dlmjugadoresCogido.addElement(jugador);
        }
    }

    /**
     *  lista el cuerpo tecnico que en general
     */
    private void listarCuerpoTecnicoEquipo() {
        dlmcuerpoTecnicocogido.clear();
        for(Cuerpotecnico cuerpotecnico:club.getListaCuerpoTecnico()){
            dlmcuerpoTecnicocogido.addElement(cuerpotecnico);
        }
    }





}
