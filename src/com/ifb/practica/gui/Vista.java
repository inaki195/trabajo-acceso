package com.ifb.practica.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.ifb.practica.base.Club;
import com.ifb.practica.base.Cuerpotecnico;
import com.ifb.practica.base.Jugador;

import javax.swing.*;

public class Vista {

    JFrame frame;
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JLabel lblnombreClub;
     JTextField txttitulos;
     JTextField txtnombreclub;
    private JLabel lblanioinaguracion;
    private JLabel lbltitulos;
    private JLabel lbllistaequipos;
     JList listaequipos;
     JButton BTNAnyadirClub;
     JButton BTNEliminarClub;
     JButton BTNObtenerInfoClub;
     JLabel lblnombre;
     JLabel lbldorsal;
     JLabel lblnacion;
     JLabel salario;
     JTextField txtnombreJugador;
     JTextField txtdorsalJugador;
     JTextField txtSalarioJugador;
     JTextField txtnacionalidad;
     JTextField txtedadJugador;
     JLabel lbledad;
     JList listajugadores;
     JButton BTNAddJugador;
     JButton BTNeliminarJugador;
     JButton BTNInfoJugador;
    private JLabel listplayers;
     JLabel lblnombrecuerpotec;
     JTextField txtnombreCuerpoTecnico;
     JTextField txtapellidoCuerpoTecnico;
    private JLabel lblapellidoctec;
    private JLabel lblocupacion;
     JTextField txtocupacionCuerpoTecnico;
     JTextField txtsalarioCuerpoTecnico;
     JLabel lblsalario;
     JLabel lbledadcuerpotec;
     JTextField txtedadCuerpoTecnico;
     JButton BTNAddCuerpotecnico;
     JButton BTNobtenerINFOCuerpoTecnico;
     JButton BTNeliminarcuerpoTecnico;
     JList listacuerpotecnico;
     DefaultListModel<Club>dlmclub;
     DefaultListModel<Cuerpotecnico>dlmcuerpotecnico;
     DefaultListModel<Cuerpotecnico>dlmcuerpotecnicoClub;
     DefaultListModel<Jugador>dlmjugadores;
     DefaultListModel<Jugador>dlmjugadoresClub;

    private JLabel lblcuerpotecnico;
    private JList listacuerpotecnicoClub;
    private JList listaJugadoresClub;
     DatePicker fechainaguracion;
     JButton BTNAplicarClub;
     JButton BTNaplicarJugador;
     JButton BTNAplicarCueerpoTecnico;
     JButton BTNrellenarClub;
     JButton BTNInfoClub;

    /**
     * genera contructor de la ventana
     */
    public Vista() {
        frame= new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        crearMenu();
        iniciarModelos();
        frame.setVisible(true);
    }
    //crea un menu

    /**
     *  creara un menu en la ventana
     */
    private void crearMenu() {
        JMenuBar barra=new JMenuBar();
        JMenu archivo=new JMenu("archivo");
        JMenuItem save=new JMenuItem("guardar como");
        JMenuItem  exportar=new JMenuItem("exportar");
        JMenuItem importar=new JMenuItem("importar");
        archivo.add(save);
        archivo.add(exportar);
        archivo.add(importar);
        barra.add(archivo);
        frame.setJMenuBar(barra);


    }



    /**
     *  lista cada jlist se le asigna el delfautlistModel
     */
    private void iniciarModelos() {
    dlmclub=new DefaultListModel<>();
    dlmcuerpotecnico=new DefaultListModel<>();
    dlmjugadores=new DefaultListModel<>();
        dlmcuerpotecnicoClub=new DefaultListModel <> ();
    dlmjugadoresClub=new DefaultListModel <> ();
    listaequipos.setModel(dlmclub);
    listacuerpotecnico.setModel(dlmcuerpotecnico);
    listajugadores.setModel(dlmjugadores);
    //lista dentro del club
    listacuerpotecnicoClub.setModel (dlmcuerpotecnicoClub);
    listaJugadoresClub.setModel (dlmjugadoresClub);
    }


}
