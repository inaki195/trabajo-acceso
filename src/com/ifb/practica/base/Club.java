package com.ifb.practica.base;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;

public class Club implements Serializable {
    /**
     *  @author ignacio fernadez bescos
       param nombrefc 1 atributo de la clase de club un String
       param anioInaguracion 2 atributo
       param tutulos variable de tipo entero
     */
    private String nombreCF;
    private LocalDate anioInaguracion;
    private int titulos;
    private HashSet<Jugador> listaJugador;
    private HashSet<Cuerpotecnico>listaCuerpoTecnico;

    /**
     *
      @param nombreCF String
      @param anioInaguracion Localdate
      @param titulos    Integer
     */
    public Club(String nombreCF, LocalDate anioInaguracion,int titulos) {
        this.nombreCF = nombreCF;
        this.anioInaguracion = anioInaguracion;
        this.titulos = titulos;
        this.listaJugador=new HashSet<>();
        this.listaCuerpoTecnico=new HashSet<>();
    }

    /**
     *
     * @return  String nombre
     */
    public String getNombreCF() {
        return nombreCF;
    }

    /**
     *
     * @param nombreCF {@link String}
     */
    public void setNombreCF(String nombreCF) {
        this.nombreCF = nombreCF;
    }

    /**
     *
     * @return Localdate anioingauracion
     */
    public LocalDate getAnioInaguracion() {
        return anioInaguracion;
    }

    /**
     *
     * @param anioInaguracion {@link LocalDate}
     */
    public void setAnioInaguracion(LocalDate anioInaguracion) {
        this.anioInaguracion = anioInaguracion;
    }

    /**
     *
     * @return titulos
     */
    public int getTitulos() {
        return titulos;
    }

    /**
     *
     *
     * @param titulos {@link Integer}
     */
    public void setTitulos(int titulos) {
        this.titulos = titulos;
    }

    /**
     *
     * @return listajugador {@link HashSet}
     */
    public HashSet<Jugador> getListaJugador() {
        return listaJugador;
    }

    /**
     *
     * @param listaJugador {@link HashSet}
     */
    public void setListaJugador(HashSet<Jugador> listaJugador) {
        this.listaJugador = listaJugador;
    }

    /**
     *
     * @return listacuerpotecnico {@link ArrayList}
     */
    public HashSet<Cuerpotecnico> getListaCuerpoTecnico() {
        return listaCuerpoTecnico;
    }

    /**
     @param listaCuerpoTecnico HashSet
     */
    public void setListaCuerpoTecnico(HashSet<Cuerpotecnico> listaCuerpoTecnico) {
        this.listaCuerpoTecnico = listaCuerpoTecnico;
    }

    /**
     *
     * @return String toString()
     */
    @Override
    public String toString() {
        return  nombreCF+" "  +anioInaguracion +" "+ titulos ;
    }
}
