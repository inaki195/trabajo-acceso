package com.ifb.practica.base;

import java.io.Serializable;

public class Jugador implements Serializable {
    private String nombre;
    private float salario;
    private int dorsal;
    private String nacionalidad;
    private float altura;

    /**
     * contructor vacio para crear objetos si parametros
     *
     */
    public Jugador() {
    }

    /**
      constructo jugador
      @param nombre String
      @param solario Float
      @param dorsal Integer
      @param nacionalidad String
      @param altura Float
     */
    public Jugador(String nombre, float solario, int dorsal, String nacionalidad, float altura) {
        this.nombre = nombre;
        this.salario = solario;
        this.dorsal = dorsal;
        this.nacionalidad = nacionalidad;
        this.altura = altura;
    }
    //creacion de getter y setter

    /**
     *
      @return nombre obtienes el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
      @param nombre cambias el nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
      @return getSalario
     */
    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    /**

      @return getDorsal
     */
    public int getDorsal() {
        return dorsal;
    }

    /**
     *
     @param  dorsal integer
     */
    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    /**
     *
    @return String
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
      @param nacionalidad String
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    /**
     *
     @return getAltura obtiene la altura del jugador
     */
    public float getAltura() {
        return altura;
    }

    /**
     * cambia la altura
      @param altura cambia altura
     */
    public void setAltura(float altura) {
        this.altura = altura;
    }
    //genero toString del jugador

    /**
      @return String
     */
    @Override
    public String toString() {
        return
                 nombre +" " +" "+salario + "€ "+dorsal + " "+ nacionalidad  + " "+altura ;
    }
}
