package com.ifb.practica.base;

import java.io.Serializable;

public class Cuerpotecnico implements Serializable {
private  String nombre;
private  String apellido;
private  String ocupacion;
private  float salario;
private  int edad;
    //

    /**
     * creacion del constructor de la clase cuerpo tecnico
     @param nombre String
      @param apellido   String
     @param ocupacion String
     @param salario Float
     @param edad Intenger
     */
    public Cuerpotecnico(String nombre, String apellido, String ocupacion, float salario, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ocupacion = ocupacion;
        this.salario = salario;
        this.edad = edad;
    }

    /**
     * obtienes apellido
      @return nombre String
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * cambia el apellido
      @param nombre String
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * obtienes el apellido
      @return apellido String
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * cambia el apellido
      @param apellido String
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * obtienes la ocupacion
      @return ocupacion
     */
    public String getOcupacion() {
        return ocupacion;
    }

    /**
     *
      @param ocupacion String
     */
    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    /**
     *
      @return salario float
     */
    public float getSalario() {
        return salario;
    }

    /**
     * cambia el salario
      @param salario Float
     */
    public void setSalario(float salario) {
        this.salario = salario;
    }

    /**
     *
      @return edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * este metodo cambia la edad
      @param edad Integer
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * genero tostring de la clase
      @return toString
     */
    @Override
    //
    public String toString() {
        return "" + " " + nombre + " " + apellido  + " '" + ocupacion  + " " + salario + "€ " + edad+" años" ;
    }
}
